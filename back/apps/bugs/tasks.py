from celery import shared_task
import requests
from django.conf import settings
from requests.auth import HTTPBasicAuth
import json


BITBUCKET_API = settings.API_URLS.get('bitbucket_url')
LOGIN_EMAIL = settings.SECRETS.get('login_email')
LOGIN_PASSWORD = settings.SECRETS.get('login_password')
DEFAULT_PROJECT = '10000'
BUG_ISSUE_TYPE_ID = '10004'


@shared_task
def resolve(id, summary):
    if set_done(id):
        return True
    new_id = create_issue(summary)
    return set_done(new_id)


@shared_task
def reopen(id, summary):
    if set_to_do(id):
        return True
    new_id = create_issue(summary)
    return set_done(new_id)


def create_issue(summary):
    url = f'{BITBUCKET_API}/rest/api/2/issue'
    auth = HTTPBasicAuth(LOGIN_EMAIL, LOGIN_PASSWORD)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    payload = json.dumps({
        'update': {},
        'fields': {
            'summary': summary,
            'project': {
                'id': DEFAULT_PROJECT
            },
            'issuetype': {
                'id': BUG_ISSUE_TYPE_ID
            }
        }
    })
    response = requests.request(
        'POST',
        url,
        data=payload,
        headers=headers,
        auth=auth
    )
    return json.loads(response.text).get('id')


def set_done(id):
    transition('31')


def set_to_do(id):
    transition('11')

def transition(transition_id):
    url = f'{BITBUCKET_API}/rest/api/3/issue/10001/transitions'
    auth = HTTPBasicAuth(LOGIN_EMAIL, LOGIN_PASSWORD)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    payload = json.dumps({
        'update': {
        },
        'fields': {},
        'transition': {
            'id': transition_id
        }
    })
    response = requests.request(
        'POST',
        url,
        data=payload,
        headers=headers,
        auth=auth
    )
    return True if response.status_code == 200 else False

def get_transitions():
    url = f'{BITBUCKET_API}/rest/api/3/issue/10001/transitions'

    auth = HTTPBasicAuth(LOGIN_EMAIL, LOGIN_PASSWORD)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }

    response = requests.request(
        'GET',
        url,
        headers=headers,
        auth=auth
    )
    return json.loads(response.text)


def search_issues(summary):
    url = f'{BITBUCKET_API}/rest/api/3/search'
    auth = HTTPBasicAuth(LOGIN_EMAIL, LOGIN_PASSWORD)
    headers = {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }
    payload = json.dumps({
        'expand': [
            'names',
            'schema',
            'operations'
        ],
        'jql': f'project = \'Demo projects\' AND summary ~ \'{summary}\'',
        'maxResults': 10,
        'fieldsByKeys': False,
        'fields': [
            'summary',
            'status',
            'assignee'
        ],
        'startAt': 0
    })
    response = requests.request(
        'POST',
        url,
        data=payload,
        headers=headers,
        auth=auth
    )
    issues = json.loads(response.text).get('issues')
    return issues
