from django.core.validators import MinValueValidator
from django.db import models


class Project(models.Model):
    title = models.CharField(max_length=255)

    def __str__(self) -> str:
        return self.title

    class Meta:
        ordering = ['title']


class Bug(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, blank=True)
    estimate = models.IntegerField(validators=[MinValueValidator(0)])
    last_update = models.DateTimeField(auto_now=True)
    project = models.ForeignKey(
        Project, on_delete=models.PROTECT, related_name='bugs')
    status = models.CharField(max_length=255)
    stared = models.BooleanField(default=False)

    def __str__(self) -> str:
        return self.title

    class Meta:
        ordering = ['title']
