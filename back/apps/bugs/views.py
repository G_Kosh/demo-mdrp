
import json
from bugs.permissions import IsAdminOrReadOnly
from bugs.pagination import DefaultPagination
from django.shortcuts import get_object_or_404
from django.db.models.aggregates import Count
from rest_framework.viewsets import ModelViewSet
from rest_framework.views import APIView, Response
from rest_framework.decorators import action
from bugs.tasks import resolve, reopen
from bugs.models import Project, Bug
from bugs.serializers import ProjectSerializer, BugSerializer, UpdateBugSerializer


class BugViewSet(ModelViewSet):
    http_method_names = ['get', 'post', 'patch', 'head', 'options']
    queryset = Bug.objects.all()
    serializer_class = BugSerializer
    pagination_class = DefaultPagination
    # permission_classes = [IsAdminOrReadOnly]
    search_fields = ['title', 'description']
    ordering_fields = ['last_update']

    # , permission_classes=[IsAdminOrReadOnly])
    @action(methods=['post'], detail=True,  url_path='resolve', url_name='resolve')
    def resolve_bug(self, request, pk=None):
        resolve.apply_async((pk, request.POST.get('title')))
        return Response({'status': 100})

    # ,  permission_classes=[IsAdminOrReadOnly])
    @action(methods=['post'], detail=True,  url_path='reopen', url_name='reopen')
    def reopen_bug(self, request, pk=None):
        reopen.apply_async((pk, request.POST.get('title')))
        return Response({'status': 100})

    def get_serializer_class(self):
        if self.request == 'PATCH':
            return UpdateBugSerializer
        return BugSerializer

    def get_serializer_context(self):
        return {'request': self.request}


class ProjectViewSet(ModelViewSet):
    queryset = Project.objects.annotate(
        Bugs_count=Count('bugs')).all()
    serializer_class = ProjectSerializer
    permission_classes = [IsAdminOrReadOnly]
