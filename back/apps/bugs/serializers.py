from decimal import Decimal
from rest_framework import serializers
from .models import Bug, Project


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ['id', 'title']


class BugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bug
        fields = ['id', 'title', 'description', 'estimate',
                  'project_id', 'last_update', 'status', 'stared']


class UpdateBugSerializer(serializers.ModelSerializer):
    class Meta:
        model = Bug
        fields = ['id', 'title', 'description', 'estimate',
                  'project_id', 'last_update', 'status', 'stared']
