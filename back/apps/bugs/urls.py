from rest_framework_nested import routers
from . import views

router = routers.DefaultRouter()
router.register('bugs', views.BugViewSet, basename='bugs')
router.register('projects', views.ProjectViewSet)

# URLConf
urlpatterns = router.urls
