import { React, Fragment } from "react";
import "./App.css";
import Bugs from "./components/bugtrack/bugs";
import Dashboard from "./components/projects/dashboard";
import Header from "./components/header";
import { storeInit } from "./store/config";
import { Provider } from "react-redux";

const store = storeInit();

function App() {
  return (
    <Provider store={store}>
      <Fragment>
        <Header />
        <Dashboard/>
        <Bugs />
      </Fragment> 
    </Provider>
  );
}

export default App;

