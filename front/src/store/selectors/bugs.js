import { createSelector } from "reselect";

export const getUnresolvedBugs = createSelector(
    state => state.entities.bugs,
    state => state.entities.projects,
    (bugs, projects, users) => bugs.list.filter(bug => bug.status.toLowerCase() !== 'resolved')
    );
    
export const getResolvedBugs = createSelector(
    state => state.entities.bugs,
    state => state.entities.projects,
    (bugs, projects, users) => bugs.list.filter(bug => bug.status.toLowerCase() === 'resolved')
);
