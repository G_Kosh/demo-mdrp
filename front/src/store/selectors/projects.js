import { createSelector } from 'reselect';

export const getUserProjects = createSelector(
    state => state.entities.projects,
    (projects, users) => projects.list.filter(project => project.id !== undefined)
    );
