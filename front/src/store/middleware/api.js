import axios from "axios";
import * as actions from "../actions/api";
import {props as config} from '../config';

const api = ({ dispatch }) => next => async action => {
  if (action.type !== actions.apiCallBegan.type) return next(action);

  const { url, method, data, onStart, onSuccess, onError } = action.payload;

  if (onStart) dispatch({ type: onStart });
  next(action);

  try {
    const response = await api_call(url, method, data)
    dispatch(actions.apiCallSuccess(response.data.results));
    const payload = response.data.results === undefined ? response.data : response.data.results
    if (onSuccess) dispatch({ type: onSuccess, payload });
  } catch (error) {
    dispatch(actions.apiCallFailed(error.message));
    if (onError) dispatch({ type: onError, payload: error.message });
  }
};

async function api_call(url, method, data) {
  console.log('Issue is here')
  console.log(process.env)
  return await axios.request({
    baseURL: config.baseAPIUrl,
    url,
    method,
    data
  });
}

export default api;
