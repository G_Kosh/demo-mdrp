import { apiCallBegan } from '../actions/api';
import moment from 'moment';
import { projects } from '../projects';

// Action Creators
const url = '/project';

export const actions = projects.actions;
export const loadProjects = () => (dispatch, getState) => {
    const { lastFetch } = getState().entities.projects;
    const diffInMinutes = moment().diff(moment(lastFetch), 'minutes');
    if (diffInMinutes < 5) return;
    return dispatch(
        apiCallBegan({
        url,
        onStart: actions.projectsRequested.type,
        onSuccess: actions.projectsReceived.type,
        onError: actions.projectsRequestFailed.type
        })
    );
};

export const deleteProject = id => apiCallBegan({
    url: url + '/' + id + '/',
    method: 'delete',
    onSuccess: actions.projectResolved.type
});
