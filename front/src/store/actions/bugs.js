import { apiCallBegan } from '../actions/api';

import moment from 'moment';
import { bugs } from '../bugs';

// Action Creators
const url = '/bug';

export const actions = bugs.actions;

export const loadBugs = () => (dispatch, getState) => {
    const { lastFetch } = getState().entities.bugs;
    const diffInMinutes = moment().diff(moment(lastFetch), 'minutes');
    if (diffInMinutes < 10) return;
    return dispatch(
        apiCallBegan({
        url,
        onStart: actions.bugsRequested.type,
        onSuccess: actions.bugsReceived.type,
        onError: actions.bugsRequestFailed.type
        })
    );
};

export const resolveBug = id => apiCallBegan({
    url: url + '/' + id + '/',
    method: 'patch',
    data: { status: 'Resolved' },
    onSuccess: actions.bugResolved.type
});

export const reopenBug = id => apiCallBegan({
    url: url + '/' + id + '/',
    method: 'patch',
    data: { status: 'Open' },
    onSuccess: actions.bugReopened.type
});

export const starBug = id => {
    return { type: actions.bugStared.type, payload: {id}}
};
