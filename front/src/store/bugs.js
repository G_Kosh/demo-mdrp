/*
This is example of complex reducer with slicer
*/
import { createSlice } from '@reduxjs/toolkit';

export const bugs = createSlice({
  name: 'bugs',
  initialState: {
    list: [],
    loading: false,
    lastFetch: null
  },
  reducers: {
    bugsRequested: (bugs, action) => {
      bugs.loading = true;
    },

    bugStared: (bugs, action) => {
      const index = bugs.list.findIndex(bug => bug.id === action.payload.id);
      bugs.list[index].stared = !bugs.list[index].stared;
    },

    bugsReceived: (bugs, action) => {
      bugs.list = action.payload;
      bugs.loading = false;
      bugs.lastFetch = Date.now();
    },

    bugsRequestFailed: (bugs, action) => {
      bugs.loading = false;
    },

    bugAssignedToUser: (bugs, action) => {
      const { id: bugId, userId } = action.payload;
      const index = bugs.list.findIndex(bug => bug.id === bugId);
      bugs.list[index].userId = userId;
    },

    bugAdded: (bugs, action) => {
      bugs.list.push(action.payload);
    },

    bugResolved: (bugs, action) => {
        const index = bugs.list.findIndex(bug => bug.id === action.payload.id);
        bugs.list[index].status = 'Resolved';
    },

    bugReopened: (bugs, action) => {
        const index = bugs.list.findIndex(bug => bug.id === action.payload.id);
        bugs.list[index].status = 'Open';
    },
  }
});
