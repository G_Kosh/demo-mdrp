import { configureStore } from '@reduxjs/toolkit';
import logger from 'redux-logger';
import {rootReducer} from '../reducers';
import api from '../middleware/api';

export const storeInit = function() {
  return configureStore({
    reducer: rootReducer,
    middleware: (getDefaultMiddleware) => getDefaultMiddleware()
      .concat(api)
      .concat(logger),
  });
};

export const props = {
  baseAPIUrl: process.env.REACT_APP_SERVER_API,
  environment: process.env.environment,
}