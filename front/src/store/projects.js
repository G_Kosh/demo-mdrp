/*
This is example of complex reducer with slicer
*/

import { createSlice } from '@reduxjs/toolkit';

export const projects = createSlice({
  name: 'projects',
  initialState: {
    list: [],
    loading: false,
    lastFetch: null
  },
  reducers: {
    projectsRequested: (projects, action) => {
      projects.loading = true;
    },

    projectsReceived: (projects, action) => {
      projects.list = action.payload;
      projects.loading = false;
      projects.lastFetch = Date.now();
    },

    projectsRequestFailed: (projects, action) => {
      projects.loading = false;
    },

    projectCreated: (projects, action) => {
      projects.list.push(action.payload);
    },

  }
});
