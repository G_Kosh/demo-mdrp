import { combineReducers } from "redux";
import { bugs } from '../bugs';
import { projects } from '../projects';

export const rootReducer = combineReducers({
    entities: combineReducers({
        bugs: bugs.reducer,
        projects: projects.reducer,
      })
  });
