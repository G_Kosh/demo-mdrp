import { useEffect, Fragment} from "react";
import {useDispatch, useSelector} from 'react-redux';
import { getUserProjects }  from '../../store/selectors/projects';
import { loadProjects }  from '../../store/actions/projects';

const Projects = () => {
    const dispatch = useDispatch()
    useEffect(() => {
        dispatch(loadProjects())
    }, [])
    const userProjects = useSelector(getUserProjects) 
    return ( 
        <Fragment>   
            <div>
                <h2>Projects List</h2>
            </div> 
            <hr class="mt-2 mb-3"/>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Phase</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {
                    userProjects.map((project) => (
                            <tr key = {project.id}>
                                <td>{project.id}</td>
                                <td>{project.name}</td>
                                <td>{project.description}</td>
                                <td>{project.phase}</td>
                                <td>
                                    <button className="btn btn-danger btn-sm">
                                        Delete
                                    </button>
                                </td>                                    
                            </tr>
                        ))
                    }
                </tbody>
            </table>    
        </Fragment>  
     );
}

export default Projects
