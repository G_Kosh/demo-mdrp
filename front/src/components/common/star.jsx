import React from 'react';

function Star(props) {
    let classes = (props.stared) ? 'fa fa-star'  : 'fa fa-star-o'
    return (
        <i 
        className={classes} 
        style={{cursor: "pointer"}}
        aria-hidden="true" 
        onClick={props.onClick}/>
    )
}

export default Star;
