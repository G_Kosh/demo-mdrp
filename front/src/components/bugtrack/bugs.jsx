import React, { useEffect, Fragment } from "react";
import { useDispatch, useSelector } from "react-redux";
import { 
    loadBugs, 
    resolveBug, 
    reopenBug, 
    starBug 
} from "../../store/actions/bugs";
import {  getUnresolvedBugs, getResolvedBugs } from "../../store/selectors/bugs";
import Star from "../common/star";

function Bugs() {
  const dispatch = useDispatch();
  const unresolvedBugs = useSelector(getUnresolvedBugs);
  const resolvedBugs = useSelector(getResolvedBugs);
  const unresolvedCount = unresolvedBugs.length;
  const resolvedCount = resolvedBugs.length;
  useEffect(() => {
    dispatch(loadBugs());
  }, []);
  return (
    <Fragment>
    <h2>Bug track</h2>
    <hr class="mt-2 mb-3"/>
    <p>Found {unresolvedCount} unresolved bugs in database</p>
    <table className="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Bug Description</th>
        <th scope="col">Estimate</th>
        <th scope="col">Status</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
    {unresolvedBugs.map((bug) => (
      <tr key={bug.id}>
        <th scope="row">{bug.id}</th>
        <td>{bug.description}</td>
        <td>{bug.estimate}</td>
        <td>{bug.status}</td>   
        <td><Star stared={bug.stared} onClick={() => dispatch(starBug(bug.id))}/></td>
        <td>
          <button className="btn btn-success btn-sm" onClick={() => dispatch(resolveBug(bug.id))}>Resolve</button>
        </td>
      </tr>
    ))}
    </tbody>
  </table>
  <p>Found {resolvedCount} resolved bugs in database</p>
    <table className="table">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Bug Description</th>
        <th scope="col">Estimate</th>
        <th scope="col">Status</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
    {resolvedBugs.map((bug) => (
      <tr key={bug.id}>
        <th scope="row">{bug.id}</th>
        <td>{bug.description}</td>
        <td>{bug.estimate}</td>
        <td>{bug.status}</td>
        <td><Star stared={bug.stared} onClick={() => dispatch(starBug(bug.id))}/></td>
        <td>
          <button className="btn btn-danger btn-sm" onClick={() => dispatch(reopenBug(bug.id))}>Reopen</button>
        </td>
      </tr>
    ))}
    </tbody>
  </table>
  </Fragment>
  )
}

export default Bugs
