insert into bugs_project (id, title)
values  (2, 'Migrations'),
        (3, 'Socail Media'),
        (4, 'Streaming Service'),
        (5, 'Dating App'),
        (6, 'Cryptocurrency'),
        (7, 'Project Management Tool'),
        (8, 'Shopp'),
        (9, 'Mobile App'),
        (10, 'IoT Firmware');
  

insert into bugs_bug (id, title, description, estimate, project_id, last_update, status, stared)
values  (1, 'Cluster', 'Status of the bug doesn\'t change in UI', 3, 4, '2020-09-11 00:00:00', 'Resolved', False),
		(2, 'Button issue', 'Resolve button doesn\'t work as expected', 8, 5, '2020-09-11 00:00:00', 'Assigned', True),
		(3, 'Performance Issue', 'New features work too slow', 6, 8, '2020-09-11 00:00:00', 'Created',  False),
		(4, 'Wrong behaviour', 'Something strange is going on', 5, 2, '2020-09-11 00:00:00', 'Invalid',  False);